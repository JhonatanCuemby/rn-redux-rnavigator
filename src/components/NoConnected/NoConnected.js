import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Button } from 'react-native';
import Connected from '../Connected/Connected';
import styles from './NoConnectedStyle';

export default class NoConnected extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>E component</Text>
        <Button
              onPress={ () => this.props.addMilk()}
              title="Dispatch"
              color="#841584">
        </Button>
          <Text> Number :
          {
            this.props.milk
          }
          </Text>
          <Connected></Connected>
      </View>
    )
  }
}