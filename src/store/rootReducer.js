import { combineReducers } from 'redux';
import milkReducer from '../containers/Milk/milkReducer';

export default combineReducers({
    milk: milkReducer
});