import { StackNavigator } from 'react-navigation';
import MilkStash from '../containers/Milk/MilkStash'
import Another from '../containers/Another/Another';
import AnotherMore from '../components/AnotherMore/AnotherMore';

export default MilkNavigator = StackNavigator(
  { Home: { screen: MilkStash },
    Another: { screen: Another },
    AnotherMore: { screen: AnotherMore }
  },
);
