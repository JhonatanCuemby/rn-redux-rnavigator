import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import NoConnected from '../../components/NoConnected/NoConnected';
import styles from './AnotherStyle';

import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

class Another extends Component {
  constructor(props){
    super(props);
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
          <Text>Another</Text>
          <Button
                onPress={ () => this.props.addMilk()}
                title="Dispatch"
                color="#841584">
          </Button>
          <Text> Number :
          {
            this.props.milk
          }
          </Text>
          <Button
              title="Go to Jane's profile"
              onPress={() =>
                navigate('AnotherMore')
              }
            />
          <NoConnected></NoConnected>
      </View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Another);