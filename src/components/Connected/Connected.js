import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Button } from 'react-native';
import { connect } from 'react-redux';
import styles from './ConnectedStyle';

import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

class Connected extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>F component</Text>
        <Button
              onPress={ () => this.props.addMilk()}
              title="Dispatch"
              color="#841584">
        </Button>
          <Text> Number :
          {
            this.props.milk
          }
          </Text>
      </View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Connected);