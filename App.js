import React, { Component } from 'react';
import { Provider } from 'react-redux';
import RootNavigator  from './src/navigators/RootNavigator';
import store from './src/store/store';

export default () => (
  <Provider store = {store}>
    <RootNavigator />
  </Provider>
);